﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Parsers.Core.Extensions;

namespace Parsers.Json.Parsers
{
    public static class RCF7159JsonParser
    {
        public static T ParseRCF7159JsonToObject<T>(this string jSon)
        {
            var dict = jSon.ParseRCF7159Json();
            return dict.GetObjectFromDictionary<T>();
        }

        public static Dictionary<string, string> ParseRCF7159Json(this string jSon)
        {
            var serializedJson = JsonConvert.SerializeObject(jSon);
            var dictionary = new Dictionary<string, string>();

            foreach (var subString in serializedJson.Split('&'))
            {
                var key = subString.Substring(0, subString.IndexOf('=')).Replace("\"", "").Replace("\\", "");
                var value = subString.Substring(subString.IndexOf('=') + 1);

                dictionary.Add(key, value);
            }

            return dictionary;
        }
    }
}
